# Ansible Role: Cumulus interfaces

This role will setup routing (FRR) on spine (and uplink) switches. Also the
interfaces will be configured and PoE will be configured on leaf switches.

Naturalis uses this role together with a private inventory.

## Requirements

None.

## Role Variables

Available variables are listed below:

An example would be:

```yaml
#group_vars
oob_gateway: 192.168.1.254
ib_mgmt: 114
fw_carp_address: x.x.x.x
default_port_config: visitors
vlans:
  114:
    name: Management
    subnet: 10.114.0.0/24
  115:
    name: Servers
    subnet: 10.115.0.0/24

port_configs:
  servers:
    untagged: 115
  office:
    untagged: 119
    tagged:
      - 120
    qos: high
  printer:
    untagged: 121
  visitors:
    untagged: 125
```

```yaml
#host_var for spine switch
eth0_mac: 44:38:39:ff:fa:01
lo: 10.255.255.6
stp_treeprio: 4096
ospf:
  vlan267:
    vlan: 267
    router_address: x.x.x.x/30
    network: 'x.x.x.x/30'
bgp:
 asn: xxx
 peer_groups:
   - name: upstream
     asn: yyy
     neighbors:
       - x.x.x.x
     prefix_lists:
       - name: pl-deny-someroutes
         direction: out
       - name: pl-deny-otherroutes
         direction: in
mlag:
  members:
    - swp47
    - swp48
  backup_ip: 192.168.144.7
cumulus_acls:
  - name: 01bgp
    protocol: bgp
    rules:
      # Allow router
      - chain: INPUT
        in_interface: swp1
        protocol: tcp
        source: x.x.x.x
        destination: x.x.x.x
        destination_port: bgp
        target: ACCEPT
svi:
  269:
    ipv4_virtual: x.x.x.x/29
    mac_virtual: xx:xx:xx:xx:xx:xx
    ipv4: x.x.x.x/29
aggregation_ports:
  swp41:
    untagged: 13
    tagged:
      - 14
      - 15
    alias: server
  swp44:
    untagged: 267
    alias: uplink
  swp45:
    untagged: 269
    alias: firewall-outside
  swp46:
    trunk: true
    alias: firewall-inside
  ser-a3:
    mlag: true
    mlag_id: 1
    members:
      - swp1
    trunk: true
  ser-a4:
    mlag: true
    mlag_id: 2
    members:
      - swp2
    trunk: true
```

```yaml
#host_var for leaf switch
eth0_mac: 44:38:39:ff:fa:02
ib_mgmt_ip: x.x.x.x
mlag:
  members:
    - swp50
  address: 169.254.1.1/30
  peer_ip: 169.254.1.2
  backup_ip: 192.168.144.24
  priority: 4096
aggregation_ports:
  uplink:
    mlag: true
    mlag_id: 1
    members:
      - swp52
  downlink:
    mlag: true
    mlag_id: 2
    members:
      - swp49
access_ports:
  swp1: servers
  swp2: office
```

## Extra variables

* `enable_omf`: By default this role disables Optimized Multicast Flooding /
  Forwarding (OMF) also known as IGMP / MLD Snooping. You can enable it by
  setting `enable_omf` to `yes`.

## Dependencies

None.

## Example Playbook

    - hosts: switches
      roles:
        - ansible-cumulus-interfaces

## License

Apache2
